#include "ball.h"
using namespace arkanoid;
using namespace game;
using namespace gameplay;
using namespace screen;
using namespace blocks;
using namespace paddle;
namespace ball
{
	ballObject ballMain;

	void ballInit()
	{
		ballMain.color = BLUE;
		ballMain.position.y = GetScreenHeight() - paddle::playerMain.paddle.height * 4 - ballMain.radius * 2;
		ballMain.collided = false;
		ballMain.active = false;
		ballMain.sfx = false;
	}
	void launch()
	{
		if (IsKeyPressed(KEY_SPACE))
		{
			ballMain.active = true;
			ballMain.speed.x = 0.0f;
			ballMain.speed.y = -300.0f;
		}
	}
	void move()
	{
		if (ballMain.active)
		{
			ballMain.position.x += ballMain.speed.x * GetFrameTime();
			ballMain.position.y += ballMain.speed.y * GetFrameTime();
		}
	}
	void checkCollisionWalls()
	{
		if (ballMain.position.x >= (GetScreenWidth() - ballMain.radius) || ballMain.position.x <= ballMain.radius)
		{
			ballMain.speed.x *= -1.0f;
		}
		if (currentScene == scene::gameplay)
		{
			if (ballMain.position.y <= ballMain.radius + reservedSpace)
			{
				ballMain.speed.y *= -1.0f;
			}
		}
		else
		{
			if (ballMain.position.y <= ballMain.radius + reservedSpace + 100)
			{
				ballMain.speed.y *= -1.0f;
			}
		}
		if (ballMain.position.y + ballMain.radius >= GetScreenHeight())
		{
			if (currentScene == scene::gameplay)
			{
				paddle::playerMain.hp--;
				paddle::checkHp();
			}
			else
			{
				ballMain.speed.y *= -1.0f;
			}
		}
	}
	void checkCollisionPaddle()
	{
		if (CheckCollisionCircleRec(ballMain.position, ballMain.radius, paddle::playerMain.paddle))
		{
			if (!ballMain.collided)
			{
				ballMain.sfx = true;
				ballMain.collided = true;
				ballMain.speed.x = ((ballMain.position.x - paddle::playerMain.paddle.x - paddle::playerMain.paddle.width / 2) / (paddle::playerMain.paddle.width / 2) * 10000) * GetFrameTime();
				ballMain.speed.y *= -1.0f;
			}
			return;
		}
		ballMain.collided = false;
	}
	void checkCollisionBlocks()
	{
		for (short i = 0; i < blocksMax; i++)
		{
			if (CheckCollisionCircleRec(ballMain.position, ballMain.radius, blocksMain[i].block))
			{
				if (blocksMain[i].active)
				{
					if (!ballMain.collided)
					{
						ballMain.collided = true;
						ballMain.speed.y *= -1.0f;
						if (blocksMain[i].destructible)
						{
							paddle::updateScore();
							blocksMain[i].hp--;
							if (blocksMain[i].hp <= 0)
							{
								blocksMain[i].active = false;
								destructibleBlocks--;
							}
						}
						return;
					}
					ballMain.collided = false;
				}
			}
		}
	}
	void followPaddle()
	{
		ballMain.position.x = paddle::playerMain.paddle.x + paddle::playerMain.paddle.width / 2;
	}
	void playPaddleHitSfx()
	{
		if (ballMain.sfx)
		{
			UpdateMusicStream(paddleHitSfx);
		}
		if (GetMusicTimePlayed(paddleHitSfx) >= GetMusicTimeLength(paddleHitSfx) - 0.1f)
		{
			ballMain.sfx = false;
		}
	}
}