#ifndef BALL
#define BALL
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "scenes/gameplay/gameplay.h"
#include "objects/paddle/paddle.h"
#include "objects/blocks/blocks.h"
namespace ball
{
	struct ballObject
	{
		Vector2 position;
		Vector2 speed;
		const float radius = 10;
		Color color;
		bool collided;
		bool active = false;
		bool sfx;
	};
	extern ballObject ballMain;

	void ballInit();
	void launch();
	void move();
	void checkCollisionWalls();
	void checkCollisionPaddle();
	void checkCollisionBlocks();
	void followPaddle();
	void playPaddleHitSfx();
}
#endif
