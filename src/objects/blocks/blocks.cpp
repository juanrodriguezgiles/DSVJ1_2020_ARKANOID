#include "blocks.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
using namespace levels;
namespace blocks
{
	static short blocksIndex;
	static const short rows = 5;
	static const short columns = 13;
	static const short blockWidht = 42;
	static const short blockHeight = 20;
	const short blocksMax = rows * columns;
	short destructibleBlocks;
	blockObject blocksMain[blocksMax];

	void blocksInit()
	{
		Color colors[rows]{ RED,YELLOW,BLUE,PURPLE,GREEN };
		destructibleBlocks = 0;
		blocksIndex = 0;

		for (short i = 0; i < rows; i++)
		{
			for (short j = 0; j < columns; j++)
			{
				switch (lvlCurrent[i][j])
				{
				case blockTypes::block:
					destructibleBlocks++;
					blocksMain[blocksIndex].color = colors[i];

					blocksMain[blocksIndex].hp = (i > 1) ? 1 : 2;

					blocksMain[blocksIndex].active = true;
					blocksMain[blocksIndex].destructible = true;

					blocksMain[blocksIndex].block.width = blockWidht;
					blocksMain[blocksIndex].block.height = blockHeight;

					blocksMain[blocksIndex].block.x = j * blocksMain[blocksIndex].block.width + 4 * j;
					blocksMain[blocksIndex].block.y = i * blocksMain[blocksIndex].block.height + blocksInitialY + 5 * i;
					break;
				case blockTypes::indestructible:
					blocksMain[blocksIndex].color = GRAY;

					blocksMain[blocksIndex].active = true;
					blocksMain[blocksIndex].destructible = false;

					blocksMain[blocksIndex].block.width = blockWidht;
					blocksMain[blocksIndex].block.height = blockHeight;

					blocksMain[blocksIndex].block.x = j * blocksMain[blocksIndex].block.width + 4 * j;
					blocksMain[blocksIndex].block.y = i * blocksMain[blocksIndex].block.height + blocksInitialY + 5 * i;
					break;
				case blockTypes::empty:
					blocksMain[blocksIndex].active = false;
					break;
				}
				blocksIndex++;
			}
		}
	}
	void blocksDraw()
	{
		for (short i = 0; i < blocksMax; i++)
		{
			if (blocksMain[i].active)
			{
#if DEBUG
				DrawRectangleLines(int(blocksMain[i].block.x), int(blocksMain[i].block.y), int(blocksMain[i].block.width + 1), int(blocksMain[i].block.height + 1), GREEN);
#endif
				DrawRectangleRec(blocksMain[i].block, blocksMain[i].color);
			}
		}
	}
}