#ifndef BLOCKS
#define BLOCKS
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "game/levels/levels.h"
namespace blocks
{
	struct blockObject
	{
		Rectangle block;
		Color color;
		short hp;
		bool active;
		bool destructible;
	};
	extern blockObject blocksMain[];
	extern const short blocksMax;
	extern short destructibleBlocks;
	void blocksInit();
	void blocksDraw();
}
#endif
