#include "paddle.h"
using namespace arkanoid;
using namespace game;
namespace paddle
{
	paddleObject playerMain;
	Texture2D paddleTexture;
	Music paddleHitSfx;
	const short maxHp = 3;

	void paddleInit()
	{
		playerMain.color = WHITE;
		playerMain.paddle.width = 100;
		playerMain.paddle.height = 20;
		playerMain.paddle.x = GetScreenWidth() / 2 - playerMain.paddle.width / 2;
		playerMain.paddle.y = GetScreenHeight() - playerMain.paddle.height * 4;
		if (playerMain.hp <= 0)
		{
			playerMain.hp = maxHp;
		}
	}
	void moveLeft()
	{
		if (playerMain.paddle.x > 0)
		{
			playerMain.paddle.x -= playerMain.xSpeed * GetFrameTime();
		}
	}
	void moveRight()
	{
		if (playerMain.paddle.x+playerMain.paddle.width < GetScreenWidth())
		{
			playerMain.paddle.x += playerMain.xSpeed * GetFrameTime();
		}
	}
	void checkHp()
	{
		if (playerMain.hp <= 0)
		{
			currentScene = scene::game_over;
		}
		else
		{
			currentScene = scene::new_round;
		}
		game::init();
	}
	void updateScore()
	{
		playerMain.score += 50;
		if (playerMain.score >= playerMain.highScore)
		{
			playerMain.highScore = playerMain.score;
		}
	}
}