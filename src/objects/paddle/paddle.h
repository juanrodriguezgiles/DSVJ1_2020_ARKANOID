#ifndef PADDLE
#define PADDLE
#include "raylib.h"
#include "game/game.h"
namespace paddle
{
	struct paddleObject
	{
		Rectangle paddle;
		Color color;
		const float xSpeed = 300.0f;
		short hp = 0;
		short score;
		short highScore;
	};
	extern paddleObject playerMain;
	extern Texture2D paddleTexture;
	extern Music paddleHitSfx; 
	extern const short maxHp;

	void paddleInit();
	void moveLeft();
	void moveRight();
	void checkHp();
	void updateScore();
}
#endif