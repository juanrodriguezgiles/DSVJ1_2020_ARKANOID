#include "main_menu.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
namespace main_menu
{
	Music mainMenuMusic;

	void init()
	{
		InitWindow(screenWidth, screenHeight, "ARKANOID");
		InitAudioDevice();
		mainMenuMusic = LoadMusicStream("res/assets/BeepBox-Song.ogg");
		PlayMusicStream(mainMenuMusic);
		SetTargetFPS(60);
		paddle::playerMain.highScore = 0;
	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			currentScene = scene::new_round;
			game::init();
		}
		if (IsKeyPressed(KEY_TWO))
		{
			currentScene = scene::how_to_play;
			game::init();
		} 
		if (IsKeyPressed(KEY_THREE))
		{
			currentScene = scene::options;
			game::init();
		}
		if (IsKeyPressed(KEY_FOUR))
		{
			currentScene = scene::credits;
			game::init();
		}
		if (IsKeyPressed(KEY_FIVE))
		{
			currentScene = scene::exitGame;
			game::init();
		}
	}
	void update()
	{
		UpdateMusicStream(mainMenuMusic);
	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("ARKANOID", (screenMiddleX - (MeasureText("ARKANOID", 50) / 2)), 30, 50, RED);
		DrawText("1. PLAY", (screenMiddleX - (MeasureText("1. PLAY", 50) / 2)), screenMiddleY / 2 - 30, 50, WHITE);
		DrawText("2. HOW TO PLAY", (screenMiddleX - (MeasureText("2. HOW TO PLAY", 50) / 2)), screenMiddleY - 80, 50, WHITE);
		DrawText("3. OPTIONS", (screenMiddleX - (MeasureText("3. OPTIONS", 50) / 2)), screenMiddleY + 20, 50, WHITE);
		DrawText("4. CREDITS", (screenMiddleX - (MeasureText("4. CREDITS", 50) / 2)), screenMiddleY + 120, 50, WHITE);
		DrawText("5. EXIT", (screenMiddleX - (MeasureText("5. EXIT", 50) / 2)), screenMiddleY + 220, 50, WHITE);
		DrawText("v0.3", 0, screenHeight - 25, 20, WHITE);
		EndDrawing();
	}
	void deInit()
	{
		UnloadTexture(paddle::paddleTexture);
		UnloadTexture(new_round::hpTexture);
		UnloadMusicStream(mainMenuMusic);
		UnloadMusicStream(paddle::paddleHitSfx);
		CloseAudioDevice();
	}
}