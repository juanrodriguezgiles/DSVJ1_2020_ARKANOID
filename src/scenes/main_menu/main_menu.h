#ifndef MAIN_MENU
#define MAIN_MENU
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/paddle/paddle.h"
namespace main_menu
{
	extern Music mainMenuMusic;

	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif
