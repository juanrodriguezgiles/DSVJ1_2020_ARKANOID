#ifndef GAME_OVER
#define GAME_OVER
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "game/levels/levels.h"
#include "objects/paddle/paddle.h"
namespace game_over
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif