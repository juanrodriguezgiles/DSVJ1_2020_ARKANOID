#include "game_over.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
using namespace levels;
namespace game_over
{
	void init()
	{

	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			currentScene = scene::new_round;
			game::init();
		}
		if (IsKeyPressed(KEY_TWO))
		{
			currentScene = scene::main_menu;
		}
		currentLevel = 1;
		paddle::playerMain.score = 0;
	}
	void update()
	{

	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("GAME OVER", (screenMiddleX - (MeasureText("GAME OVER", 50) / 2)), screenMiddleY / 2, 50, WHITE);
		DrawText("1. PLAY", (screenMiddleX - (MeasureText("1. PLAY", 50) / 2)), screenMiddleY, 50, WHITE);
		DrawText("2. MAIN MENU", (screenMiddleX - (MeasureText("2. MAIN MENU", 50) / 2)), screenMiddleY + 100, 50, WHITE);
		EndDrawing();
	}
	void deInit()
	{

	}
}