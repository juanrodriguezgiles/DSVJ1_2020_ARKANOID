#ifndef EXIT
#define EXIT
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
namespace exitGame
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif