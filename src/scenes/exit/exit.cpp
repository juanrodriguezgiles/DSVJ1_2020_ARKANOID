#include "exit.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
namespace exitGame
{
	void init()
	{

	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			currentScene = scene::main_menu;
		}
		if (IsKeyPressed(KEY_TWO))
		{
			game::deInit();
			playing = false;
		}
	}
	void update()
	{

	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("EXIT?", (screenMiddleX - (MeasureText("EXIT?", 50) / 2)), screenMiddleY / 2, 50, WHITE);
		DrawText("1. NO", (screenMiddleX - (MeasureText("1. NO", 50) / 2)), screenMiddleY, 50, WHITE);
		DrawText("2. YES", (screenMiddleX - (MeasureText("2. YES", 50) / 2)), screenMiddleY + 100, 50, WHITE);
		EndDrawing();
	}
	void deInit()
	{

	}
}