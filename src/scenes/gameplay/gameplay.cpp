#include "gameplay.h"
using namespace arkanoid;
using namespace game;
using namespace controls;
using namespace screen;
using namespace levels;
using namespace new_round;
using namespace paddle;
using namespace ball;
using namespace blocks;
namespace gameplay
{
	void init()
	{
		paddleHitSfx = LoadMusicStream("res/assets/paddle_hit.ogg");
		PlayMusicStream(paddleHitSfx);
	}	
	void input()
	{
		if (IsKeyDown(left))
		{
			moveLeft();
		}
		if (IsKeyDown(right))
		{
			moveRight();
		}
		if (IsKeyDown(pauseGame))
		{
			currentScene = scene::pause;
		}
	}
	void update()
	{
		move();
		checkCollisionWalls();
		checkCollisionPaddle();
		checkCollisionBlocks();
		playPaddleHitSfx();
		checkWin();
	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
#if DEBUG
		DrawRectangleLines(int(playerMain.paddle.x), int(playerMain.paddle.y), int(playerMain.paddle.width + 1), int(playerMain.paddle.height + 1), GREEN);
		DrawCircleLines(int(ballMain.position.x), int(ballMain.position.y), ballMain.radius, GREEN);
#endif
		DrawTexture(paddleTexture, int(playerMain.paddle.x), int(playerMain.paddle.y), WHITE);
		DrawCircleV(ballMain.position, ballMain.radius, ballMain.color);
		DrawText("SCORE", screenMiddleX - 150, reservedSpace / 3, 25, WHITE);
		DrawText(FormatText("%01i", playerMain.score), screenMiddleX - 50, reservedSpace / 3, 25, WHITE);
		DrawText("HIGH SCORE", screenMiddleX + 50, reservedSpace / 3, 25, WHITE);
		DrawText(FormatText("%01i", playerMain.highScore), (screenMiddleX + 70 + (MeasureText("HIGH SCORE", 25))), reservedSpace / 3, 25, WHITE);
		DrawLine(0, reservedSpace, GetScreenWidth(), reservedSpace, WHITE);
		if (playerMain.hp == maxHp)
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width * 2, reservedSpace - hpTexture.height - 5, WHITE);
		}
		else if (playerMain.hp == 2)
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width, reservedSpace - hpTexture.height - 5, WHITE);
		}
		else
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
		}
		blocksDraw();
		EndDrawing();
	}
	void deInit()
	{
	}
}