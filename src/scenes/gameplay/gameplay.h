#ifndef GAMEPLAY
#define GAMEPLAY
#include "raylib.h"
#include "game/game.h"
#include "game/controls/controls.h"
#include "game/screen/screen.h"
#include "game/levels/levels.h"
#include "scenes/new_round/new_round.h"
#include "objects/ball/ball.h"
#include "objects/paddle/paddle.h"
#include "objects/blocks/blocks.h"
namespace gameplay
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif
