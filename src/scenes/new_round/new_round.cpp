#include "new_round.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
using namespace levels;
using namespace ball;
using namespace paddle;
using namespace blocks;
namespace new_round
{
	Texture2D hpTexture;
	
	void init()
	{
		hpTexture = LoadTexture("res/assets/hp.png");
		paddleTexture = LoadTexture("res/assets/paddle.png");
		paddleInit();
		ballInit();
		if (playerMain.hp == maxHp)
		{
			loadLevel();
			blocksInit();
		}
	}
	void input()
	{
		if (IsKeyDown(KEY_LEFT))
		{
			moveLeft();
		}
		if (IsKeyDown(KEY_RIGHT))
		{
			moveRight();
		}
		if (IsKeyPressed(KEY_SPACE))
		{
			currentScene = scene::gameplay;
			game::init();
			launch();
		}
	}
	void update()
	{
		followPaddle();
	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
#if DEBUG
		DrawRectangleLines(int(playerMain.paddle.x), int(playerMain.paddle.y), int(playerMain.paddle.width + 1), int(playerMain.paddle.height + 1), GREEN);
		DrawCircleLines(int(ballMain.position.x), int(ballMain.position.y), ballMain.radius, GREEN);
#endif
		DrawTexture(paddleTexture, int(playerMain.paddle.x), int(playerMain.paddle.y), WHITE);
		DrawCircleV(ballMain.position, ballMain.radius, ballMain.color);
		DrawText("SCORE", screenMiddleX - 150, reservedSpace / 3, 25, WHITE);
		DrawText(FormatText("%01i", playerMain.score), screenMiddleX - 50, reservedSpace / 3, 25, WHITE);
		DrawText("HIGH SCORE", screenMiddleX + 50, reservedSpace / 3, 25, WHITE);
		DrawText(FormatText("%01i", playerMain.highScore), (screenMiddleX + 70 + (MeasureText("HIGH SCORE", 25))), reservedSpace / 3, 25, WHITE);
		DrawText("PRESS SPACE TO START", (screenMiddleX - (MeasureText("PRESS SPACE TO START", 40) / 2)), screenMiddleY + 50, 40, WHITE);
		DrawLine(0, reservedSpace, GetScreenWidth(), reservedSpace, WHITE);
		if (playerMain.hp == maxHp)
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width * 2, reservedSpace - hpTexture.height - 5, WHITE);
		}
		else if (playerMain.hp == 2)
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width, reservedSpace - hpTexture.height - 5, WHITE);
		}
		else
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
		}
		blocksDraw();
		EndDrawing();
	}
	void deInit()
	{

	}
}