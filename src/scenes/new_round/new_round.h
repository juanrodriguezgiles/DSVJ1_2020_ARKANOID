#ifndef NEW_ROUND
#define NEW_ROUND
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "game/levels/levels.h"
#include "objects/paddle/paddle.h"
#include "objects/ball/ball.h"
#include "objects/blocks/blocks.h"
namespace new_round
{
	extern Texture2D hpTexture;

	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif