#include "pause.h"
using namespace arkanoid;
using namespace game;
using namespace levels;
using namespace screen;
using namespace new_round;
using namespace paddle;
using namespace ball;
using namespace blocks;
namespace pause
{
	void init()
	{

	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			currentScene = scene::gameplay;
		}
		if (IsKeyPressed(KEY_TWO))
		{
			currentLevel = 1;
			playerMain.hp = maxHp;
			playerMain.score = 0;
			currentScene = scene::main_menu;
		}
	}
	void update()
	{

	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawTexture(paddleTexture, int(playerMain.paddle.x), int(playerMain.paddle.y), WHITE);
		DrawText("SCORE", screenMiddleX - 150, reservedSpace / 3, 25, WHITE);
		DrawText(FormatText("%01i", playerMain.score), screenMiddleX - 50, reservedSpace / 3, 25, WHITE);
		DrawText("HIGH SCORE", screenMiddleX + 50, reservedSpace / 3, 25, WHITE);
		DrawText(FormatText("%01i", playerMain.highScore), (screenMiddleX + 70 + (MeasureText("HIGH SCORE", 25))), reservedSpace / 3, 25, WHITE);
		DrawText("PAUSE", (screenMiddleX - (MeasureText("PAUSE", 40) / 2)), screenMiddleY / 2 + 100, 40, WHITE);
		DrawText("1. RESUME", (screenMiddleX - (MeasureText("1. RESUME", 40) / 2)), screenMiddleY / 2 + 150, 40, WHITE);
		DrawText("2. MAIN MENU", (screenMiddleX - (MeasureText("2. MAIN MENU", 40) / 2)), screenMiddleY / 2 + 200, 40, WHITE);
		DrawLine(0, reservedSpace, GetScreenWidth(), reservedSpace, WHITE);
		if (playerMain.hp == maxHp)
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width * 2, reservedSpace - hpTexture.height - 5, WHITE);
		}
		else if (playerMain.hp == 2)
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
			DrawTexture(hpTexture, hpTexture.width, reservedSpace - hpTexture.height - 5, WHITE);
		}
		else
		{
			DrawTexture(hpTexture, 0, reservedSpace - hpTexture.height - 5, WHITE);
		}
		blocksDraw();
		EndDrawing();
	}
	void deInit()
	{

	}
}