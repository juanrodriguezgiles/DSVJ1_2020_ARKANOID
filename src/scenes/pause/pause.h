#ifndef PAUSE
#define PAUSE
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "game/levels/levels.h"
#include "scenes/new_round/new_round.h"
#include "objects/paddle/paddle.h"
#include "objects/ball/ball.h"
#include "objects/blocks/blocks.h"
namespace pause
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif
