#ifndef NEW_LEVEL
#define NEW_LEVEL
#include "raylib.h"
#include "game/game.h"
#include "game/levels/levels.h"
#include "game/screen/screen.h"
#include "objects/paddle/paddle.h"
namespace new_level
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif