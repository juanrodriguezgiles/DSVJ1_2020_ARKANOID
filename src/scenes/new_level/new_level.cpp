#include "new_level.h"
using namespace arkanoid;
using namespace game;
using namespace levels;
using namespace screen;
using namespace paddle;
namespace new_level
{
	void init()
	{

	}
	void input()
	{
		if (currentLevel != maxLevel + 1)
		{
			if (IsKeyPressed(KEY_ONE))
			{
				playerMain.hp = maxHp;
				currentScene = scene::new_round;
				game::init();
			}
			if (IsKeyPressed(KEY_TWO))
			{
				currentLevel = 1;
				currentScene = scene::main_menu;
			}
		}
		else
		{
			if (IsKeyPressed(KEY_ONE))
			{
				currentLevel = 1;
				currentScene = scene::main_menu;
			}
		}
	}
	void update()
	{

	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		if (currentLevel != maxLevel + 1)
		{
			DrawText("LEVEL WON!", (screenMiddleX - (MeasureText("LEVEL WON!", 50) / 2)), screenMiddleY / 2, 50, WHITE);
			DrawText("1. NEXT LEVEL", (screenMiddleX - (MeasureText("1. NEXT LEVEL", 50) / 2)), screenMiddleY / 2 + 100, 50, WHITE);
			DrawText("2. MAIN MENU", (screenMiddleX - (MeasureText("2. MAIN MENU", 50) / 2)), screenMiddleY / 2 + 200, 50, WHITE);
		}
		else
		{
			DrawText("ALL LEVELS COMPLETED!", (screenMiddleX - (MeasureText("ALL LEVELS COMPLETED!", 40) / 2)), screenMiddleY / 2, 40, WHITE);
			DrawText("1. MAIN MENU", (screenMiddleX - (MeasureText("1. MAIN MENU", 30) / 2)), screenMiddleY + 50, 30, WHITE);
		}
		EndDrawing();
	}
	void deInit()
	{

	}
}