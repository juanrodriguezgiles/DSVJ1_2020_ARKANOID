#ifndef CREDITS
#define CREDITS
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
namespace credits
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif
