#include "credits.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
namespace credits
{
	void init()
	{

	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			currentScene = scene::main_menu;
		}
	}
	void update()
	{

	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("CREDITS", (screenMiddleX - (MeasureText("CREDITS", 50) / 2)), screenMiddleY / 2, 50, WHITE);
		DrawText("DESIGNED AND CODED BY JUAN RODRIGUEZ GILES", (screenMiddleX - (MeasureText("DESIGNED AND CODED BY JUAN RODRIGUEZ GILES", 22) / 2)), screenMiddleY, 22, WHITE);
		DrawText("1. MAIN MENU", (screenMiddleX - (MeasureText("1. MAIN MENU", 30) / 2)), screenHeight - 50, 30, WHITE);
		EndDrawing();
	}
	void deInit()
	{

	}
}