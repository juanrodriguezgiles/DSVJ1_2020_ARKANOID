#ifndef HOW_TO_PLAY
#define HOW_TO_PLAY
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/paddle/paddle.h"
#include "objects/ball/ball.h"
#include "objects/blocks/blocks.h"
namespace how_to_play
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif
