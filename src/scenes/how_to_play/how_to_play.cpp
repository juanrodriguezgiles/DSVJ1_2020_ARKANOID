#include "how_to_play.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
using namespace paddle;
using namespace ball;
using namespace blocks;
namespace how_to_play
{
	static bool launched;

	void init()
	{
		launched = false;
		paddleInit();
		ballInit();
	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			currentScene = scene::main_menu;
		}
		if (IsKeyPressed(KEY_SPACE) && !launched)
		{
			launch();
			launched = true;
		}
		if (IsKeyDown(KEY_LEFT))
		{
			moveLeft();
		}
		if (IsKeyDown(KEY_RIGHT))
		{
			moveRight();
		}
	}
	void update()
	{
		if (!launched)
		{
			followPaddle();
		}
		move();
		checkCollisionWalls();
		checkCollisionPaddle();
	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("MOVE WITH THE LEFT AND RIGHT ARROW KEYS", (screenMiddleX - (MeasureText("MOVE WITH THE LEFT AND RIGHT ARROW KEYS", 20) / 2)), 20, 20, WHITE);
		DrawText("PRESS SPACEBAR TO LAUNCH THE BALL", (screenMiddleX - (MeasureText("PRESS SPACEBAR TO LAUNCH THE BALL", 20) / 2)), 60, 20, WHITE);
		DrawText("PRESS P TO PAUSE", (screenMiddleX - (MeasureText("PRESS P TO PAUSE", 20) / 2)), 100, 20, WHITE);
		DrawRectangleRec(playerMain.paddle, playerMain.color);
		DrawCircleV(ballMain.position, ballMain.radius, ballMain.color);
		DrawText("1. MAIN MENU", (screenMiddleX - (MeasureText("1. MAIN MENU", 30) / 2)), screenHeight - 50, 30, WHITE);
		EndDrawing();
	}
	void deInit()
	{

	}
}