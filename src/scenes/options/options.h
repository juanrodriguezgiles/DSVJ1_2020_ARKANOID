#ifndef OPTIONS
#define OPTIONS
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "scenes/main_menu/main_menu.h"
#include "scenes/gameplay/gameplay.h"
namespace options
{
	void init();
	void input();
	void update();
	void draw();
	void deInit();
}
#endif