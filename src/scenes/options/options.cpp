#include "options.h"
using namespace arkanoid;
using namespace game;
using namespace screen;
using namespace main_menu;
using namespace gameplay;
using namespace paddle;
namespace options
{
	static float volume = 1.0f;

	static void increaseVolume()
	{
		if (volume < 0.9f)
		{
			volume += 0.1f;
		}
		SetMusicVolume(mainMenuMusic, volume);
		SetMusicVolume(paddleHitSfx, volume);
	}
	static void decreaseVolume()
	{
		if (volume > 0.1f)
		{
			volume -= 0.1f;
		}
		SetMusicVolume(mainMenuMusic, volume);
		SetMusicVolume(paddleHitSfx, volume);
	}
	void init()
	{

	}
	void input()
	{
		if (IsKeyPressed(KEY_ONE))
		{
			increaseVolume();
		}
		if (IsKeyPressed(KEY_TWO))
		{
			decreaseVolume();
		}
		if (IsKeyPressed(KEY_THREE))
		{
			currentScene = scene::main_menu;
		}
	}
	void update()
	{

	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("OPTIONS", (screenMiddleX - (MeasureText("OPTIONS", 50) / 2)), screenMiddleY / 10, 50, WHITE);
		DrawText("1. INCREASE VOLUME", (screenMiddleX - (MeasureText("1. INCREASE VOLUME", 40) / 2)), screenMiddleY / 2, 40, WHITE);
		DrawText("2. DECREASE VOLUME", (screenMiddleX - (MeasureText("2. DECREASE VOLUME", 40) / 2)), screenMiddleY / 2 + 70, 40, WHITE);
		DrawText("3. MAIN MENU", (screenMiddleX - (MeasureText("3. MAIN MENU", 30) / 2)), screenHeight - 50, 30, WHITE);
		EndDrawing();
	}
	void deInit()
	{

	}
}