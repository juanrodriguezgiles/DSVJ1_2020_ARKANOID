#include "game.h"
namespace arkanoid
{
	namespace game
	{
		scene currentScene = scene::main_menu;
		bool playing = true;

		void init()
		{
			switch (currentScene)
			{
			case arkanoid::game::scene::main_menu:
				main_menu::init();
				break;
			case arkanoid::game::scene::options:
				options::init();
				break;
			case arkanoid::game::scene::credits:
				credits::init();
				break;
			case arkanoid::game::scene::gameplay:
				gameplay::init();
				break;
			case arkanoid::game::scene::pause:
				pause::init();
				break;
			case arkanoid::game::scene::exitGame:
				exitGame::init();
				break;
			case arkanoid::game::scene::how_to_play:
				how_to_play::init();
				break;
			case arkanoid::game::scene::new_round:
				new_round::init();
				break;
			case arkanoid::game::scene::game_over:
				game_over::init();
				break;
			case arkanoid::game::scene::new_level:
				new_level::init();
				break;
			}
		}
		static void input()
		{
			switch (currentScene)
			{
			case arkanoid::game::scene::main_menu:
				main_menu::input();
				break;
			case arkanoid::game::scene::options:
				options::input();
				break;
			case arkanoid::game::scene::credits:
				credits::input();
				break;
			case arkanoid::game::scene::gameplay:
				gameplay::input();
				break;
			case arkanoid::game::scene::pause:
				pause::input();
				break;
			case arkanoid::game::scene::exitGame:
				exitGame::input();
				break;
			case arkanoid::game::scene::how_to_play:
				how_to_play::input();
				break;
			case arkanoid::game::scene::new_round:
				new_round::input();
				break;
			case arkanoid::game::scene::game_over:
				game_over::input();
				break;
			case arkanoid::game::scene::new_level:
				new_level::input();
				break;
			}
		}
		static void update()
		{
			switch (currentScene)
			{
			case arkanoid::game::scene::main_menu:
				main_menu::update();
				break;
			case arkanoid::game::scene::options:
				options::update();
				break;
			case arkanoid::game::scene::credits:
				credits::update();
				break;
			case arkanoid::game::scene::gameplay:
				gameplay::update();
				break;
			case arkanoid::game::scene::pause:
				pause::update();
				break;
			case arkanoid::game::scene::exitGame:
				exitGame::update();
				break;
			case arkanoid::game::scene::how_to_play:
				how_to_play::update();
				break;
			case arkanoid::game::scene::new_round:
				new_round::update();
				break;
			case arkanoid::game::scene::game_over:
				game_over::update();
				break;
			case arkanoid::game::scene::new_level:
				new_level::update();
				break;
			}
		}
		static void draw()
		{
			switch (currentScene)
			{
			case arkanoid::game::scene::main_menu:
				main_menu::draw();
				break;
			case arkanoid::game::scene::options:
				options::draw();
				break;
			case arkanoid::game::scene::credits:
				credits::draw();
				break;
			case arkanoid::game::scene::gameplay:
				gameplay::draw();
				break;
			case arkanoid::game::scene::pause:
				pause::draw();
				break;
			case arkanoid::game::scene::exitGame:
				exitGame::draw();
				break;
			case arkanoid::game::scene::how_to_play:
				how_to_play::draw();
				break;
			case arkanoid::game::scene::new_round:
				new_round::draw();
				break;
			case arkanoid::game::scene::game_over:
				game_over::draw();
				break;
			case arkanoid::game::scene::new_level:
				new_level::draw();
				break;
			}
		}
		void deInit()
		{
			switch (currentScene)
			{
			case arkanoid::game::scene::main_menu:
				main_menu::deInit();
				break;
			case arkanoid::game::scene::options:
				options::deInit();
				break;
			case arkanoid::game::scene::credits:
				credits::deInit();
				break;
			case arkanoid::game::scene::gameplay:
				gameplay::deInit();
				break;
			case arkanoid::game::scene::pause:
				pause::deInit();
				break;
			case arkanoid::game::scene::exitGame:
				exitGame::deInit();
				break;
			case arkanoid::game::scene::how_to_play:
				how_to_play::deInit();
				break;
			case arkanoid::game::scene::new_round:
				new_round::deInit();
				break;
			case arkanoid::game::scene::game_over:
				game_over::deInit();
				break;
			case arkanoid::game::scene::new_level:
				new_level::deInit();
				break;
			}
		}
		void run()
		{
			while (!WindowShouldClose() && playing)
			{
				input();
				update();
				draw();
			}
		}
	}
}