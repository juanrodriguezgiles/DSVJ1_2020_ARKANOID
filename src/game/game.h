#ifndef GAME_H
#define GAME_H
#include "raylib.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/main_menu/main_menu.h"
#include "scenes/new_round/new_round.h"
#include "scenes/game_over/game_over.h"
#include "scenes/pause/pause.h"
#include "scenes/exit/exit.h"
#include "scenes/credits/credits.h"
#include "scenes/options/options.h"
#include "scenes/how_to_play/how_to_play.h"
#include "scenes/new_level/new_level.h"
namespace arkanoid
{
	namespace game
	{
		enum class scene
		{
			main_menu,
			options,
			credits,
			gameplay,
			pause,
			exitGame,
			how_to_play,
			new_round,
			game_over,
			new_level
		};
		extern scene currentScene;
		extern bool playing;

		void run();
		void init();
		void deInit();
	}
}
#endif