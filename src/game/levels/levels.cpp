#include "levels.h"
using namespace blocks;
namespace levels
{
	static const short rows = 5;
	static const short columns = 13;
	short currentLevel = 1;
	const short maxLevel = 3;
	blockTypes lvlCurrent[rows][columns];

	static blockTypes lvl1[rows][columns] =
	{ {blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block},
		{ blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block },
		{ blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block },
		{ blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block },
		{ blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block }
	};

	static blockTypes lvl2[rows][columns] =
	{ {blockTypes::empty, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::indestructible, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::empty},
	{ blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty },
	{ blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty },
	{ blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty },
	{ blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty }
	};

	static blockTypes lvl3[rows][columns] =
	{ {blockTypes::indestructible, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::empty, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::block, blockTypes::indestructible},
	{ blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible },
	{ blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible },
	{ blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible },
	{ blockTypes::indestructible,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::empty,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::block,blockTypes::indestructible }
	};

	void loadLevel()
	{
		for (short i = 0; i < rows; i++)
		{
			for (short j = 0; j < columns; j++)
			{
				switch (currentLevel)
				{
				case 1:
					lvlCurrent[i][j] = lvl1[i][j];
					break;
				case 2:
					lvlCurrent[i][j] = lvl2[i][j];
					break;
				case 3:
					lvlCurrent[i][j] = lvl3[i][j];
				}
			}
		}
	}
	void checkWin()
	{
		if (destructibleBlocks <= 0)
		{
			currentLevel++;
			arkanoid::game::currentScene = arkanoid::game::scene::new_level;
			arkanoid::game::init();
		}
	}
}