#ifndef LEVELS
#define LEVELS
#include "raylib.h"
#include "game/game.h"
#include "objects/blocks/blocks.h"
namespace levels
{
	enum class blockTypes
	{
		empty,
		block,
		indestructible
	};
	extern short currentLevel;
	extern const short maxLevel;
	extern blockTypes lvlCurrent[5][13];

	void loadLevel();
	void checkWin();
}
#endif