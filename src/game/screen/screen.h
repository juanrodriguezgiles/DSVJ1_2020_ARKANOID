#ifndef SCREEN
#define SCREEN
#include "raylib.h"
namespace screen
{
	extern const int screenWidth;
	extern const int screenHeight;
	extern const int reservedSpace;
	extern const int screenMiddleX;
	extern const int screenMiddleY;
	extern const int blocksInitialY;
}
#endif