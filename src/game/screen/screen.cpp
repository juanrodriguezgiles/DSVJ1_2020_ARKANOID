#include "screen.h"
namespace screen
{
	const int screenWidth = 600;
	const int screenHeight = 600;
	const int reservedSpace = 50;
	const int screenMiddleX = screenWidth / 2;
	const int screenMiddleY = screenHeight / 2;
	const int blocksInitialY = reservedSpace + 51;
}